![Heimdallr logo](https://gitlab.com/NorwegianLanguageLearning/heimdallr/raw/master/heimdallr_avatar.svg)
# Heimdallr
[![pipeline status](https://gitlab.com/NorwegianLanguageLearning/heimdallr/badges/master/pipeline.svg)](https://gitlab.com/NorwegianLanguageLearning/heimdallr/commits/master)

Heimdallr is a Discord bot, mainly made for managing the Norwegian Language Learning Discord server.

## Planned features
- [x] Notify chat when new members join the server, and when members leave.
- [x] DM new members with information regarding the server.
- [x] Provide an information message in an admin chat when a user gets banned, provide username + user ID.
- [x] Ban users through a command
- [x] Kick users through a command
- [x] Set up an infraction/warning system
- [ ] Notify the chat about events (e.g. the server's weekly calls)
- [x] Quote users by message ID using an Embed.
- [ ] ... (more stuff to come!)
