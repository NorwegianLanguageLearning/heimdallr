package commands

import (
	"github.com/bwmarrin/discordgo"
	heimdallr "gitlab.com/NorwegianLanguageLearning/heimdallr/bot"
)

func OnVoiceStateUpdate(s *discordgo.Session, v *discordgo.VoiceStateUpdate) {
	if heimdallr.Config.InVoiceRole == "" {
		return
	}
	var err error

	if v.ChannelID == "" {
		err = s.GuildMemberRoleRemove(v.GuildID, v.UserID, heimdallr.Config.InVoiceRole)
	} else {
		err = s.GuildMemberRoleAdd(v.GuildID, v.UserID, heimdallr.Config.InVoiceRole)
	}

	if err != nil {
		heimdallr.LogIfError(s, err)
	}
}
