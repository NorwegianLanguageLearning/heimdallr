package commands

import (
	"fmt"
	"log/slog"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	heimdallr "gitlab.com/NorwegianLanguageLearning/heimdallr/bot"
)

var kickCommand = command{
	"kick",
	commandKickUser,
	"Kicks a user from the server.",
	[]string{
		"<user> <reason>...",
	},
	[]string{
		"@username \"Did something wrong\"",
		"245267597929480102 \"Did something wrong\"",
	},
}

// commandKickUser kicks a user from the server.
func commandKickUser(s *discordgo.Session, m *discordgo.MessageCreate, args docopt.Opts) error {
	userID := getIDFromMaybeMention(args["<user>"].(string))
	reason := strings.Join(args["<reason>"].([]string), " ")

	guildID := m.GuildID
	member, err := heimdallr.GetMember(s, guildID, userID)
	if err != nil {
		_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("No member was found with ID %s.", userID))
		return errors.Wrap(err, "sending message failed")
	}
	user := member.User
	if userID == s.State.User.ID {
		_, err := s.ChannelMessageSend(m.ChannelID, "I'm not going to kick myself, silly. 😉")
		return errors.Wrap(err, "sending message failed")
	}

	msgUserErr := sendKickMessage(userID, reason, s, m)

	err = s.GuildMemberDeleteWithReason(guildID, userID, reason)
	if err != nil {
		return errors.Wrap(err, "kick failed")
	}

	err = heimdallr.AddInfraction(*user, heimdallr.Infraction{
		Reason: fmt.Sprintf("Received a kick with reason: %s", reason),
		Time:   time.Now(),
	})
	if err != nil {
		return err
	}

	if msgUserErr != nil {
		slog.Warn("Failed to message user for kick.", "user_id", userID, "err", msgUserErr)
		_, err = s.ChannelMessageSendReply(m.ChannelID, "Failed to message user.", m.Reference())
		if err != nil {
			return fmt.Errorf("failed to warn about unsuccessful message: %w", err)
		}
	}

	err = s.MessageReactionAdd(m.ChannelID, m.ID, "✅")
	return errors.Wrap(err, "adding reaction failed")
}

func sendKickMessage(userID, reason string, s *discordgo.Session, m *discordgo.MessageCreate) error {
	guild, err := heimdallr.GetGuild(s, m.GuildID)
	if err != nil {
		return fmt.Errorf("failed to get guild: %w", err)
	}

	userChannel, err := s.UserChannelCreate(userID)
	if err != nil {
		slog.Warn("Failed to create private channel", "user_id", userID)
		return errors.Wrap(err, "creating private channel failed")
	}
	_, err = s.ChannelMessageSend(userChannel.ID, fmt.Sprintf(
		"You have been kicked from %s for the following reason: %s\n\nYou cannot reply to this message.",
		guild.Name, reason,
	))
	if err != nil {
		slog.Warn("Failed to DM user", "user_id", userID)
		return errors.Wrap(err, "sending message failed")
	}

	return nil
}
