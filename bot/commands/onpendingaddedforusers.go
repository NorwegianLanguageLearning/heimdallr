package commands

import (
	"log/slog"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	heimdallr "gitlab.com/NorwegianLanguageLearning/heimdallr/bot"
)

func OnPendingAddedForUsers(s *discordgo.Session, gmu *discordgo.GuildMemberUpdate) {
	gal, err := s.GuildAuditLog(gmu.GuildID, gmu.User.ID, "", int(discordgo.AuditLogActionMemberRoleUpdate), 1)
	if err != nil {
		heimdallr.LogIfError(s, errors.Wrap(err, "Failed to get Audit Log"))
		return
	}
	if len(gal.AuditLogEntries) == 0 {
		return
	}

	entry := gal.AuditLogEntries[0]

	if entry.TargetID != entry.UserID {
		return
	}

	for _, change := range entry.Changes {
		if *change.Key != discordgo.AuditLogChangeKeyRoleAdd {
			continue
		}

		newValues, ok := change.NewValue.([]interface{})
		if !ok || len(newValues) != 1 {
			continue
		}

		newValue, ok := newValues[0].(map[string]interface{})
		if !ok {
			continue
		}

		newRoleId, ok := newValue["id"].(string)
		if !ok {
			continue
		}

		if newRoleId == heimdallr.Config.PendingRole {
			if isApproved(gmu.Member) {
				if err = s.GuildMemberRoleRemove(gmu.GuildID, gmu.User.ID, heimdallr.Config.PendingRole); err != nil {
					slog.Error("Failed to remove role", "err", err)
				}

			}
			return
		}
	}

}
