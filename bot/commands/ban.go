package commands

import (
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	heimdallr "gitlab.com/NorwegianLanguageLearning/heimdallr/bot"
)

var banCommand = command{
	"ban",
	commandBanUser,
	"Bans a user from the server.",
	[]string{
		"<user> <reason>...",
	},
	[]string{
		"@username \"Did something wrong\"",
		"245167597929480192 \"Did something wrong\"",
	},
}

// commandBanUser bans a user from the server.
func commandBanUser(s *discordgo.Session, m *discordgo.MessageCreate, args docopt.Opts) error {
	userID := getIDFromMaybeMention(args["<user>"].(string))
	reason := strings.Join(args["<reason>"].([]string), " ")

	guildID := m.GuildID
	member, err := heimdallr.GetMember(s, guildID, userID)
	var user *discordgo.User
	if err != nil {
		user, err = s.User(userID)
		if err != nil {
			_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("No user was found with ID %s.", userID))
			return errors.Wrap(err, "sending message failed")
		}
	} else {
		user = member.User
	}
	if userID == s.State.User.ID {
		_, err := s.ChannelMessageSend(m.ChannelID, "I'm not going to ban myself, silly. 😉")
		return errors.Wrap(err, "sending message failed")
	}

	msgUserErr := sendBanMessage(userID, reason, s, m)

	err = s.GuildBanCreateWithReason(guildID, userID, reason, 0)
	if err != nil {
		return errors.Wrap(err, "ban failed")
	}
	err = heimdallr.AddInfraction(*user, heimdallr.Infraction{
		Reason: fmt.Sprintf("Received a ban with reason: %s", reason),
		Time:   time.Now(),
	})
	if err != nil {
		return err
	}

	if msgUserErr != nil {
		_, err = s.ChannelMessageSendReply(m.ChannelID, "Failed to message user.", m.Reference())
		if err != nil {
			return fmt.Errorf("failed to warn about unsuccessful message: %w", err)
		}
	}

	err = s.MessageReactionAdd(m.ChannelID, m.ID, "✅")
	return errors.Wrap(err, "adding reaction failed")
}

func sendBanMessage(userID, reason string, s *discordgo.Session, m *discordgo.MessageCreate) error {
	guild, err := heimdallr.GetGuild(s, m.GuildID)
	if err != nil {
		return fmt.Errorf("failed to get guild: %w", err)
	}

	userChannel, err := s.UserChannelCreate(userID)
	if err != nil {
		return errors.Wrap(err, "creating private channel failed")
	}
	_, err = s.ChannelMessageSend(userChannel.ID, fmt.Sprintf(
		"You have been banned from %s for the following reason: %s\n\nYou cannot reply to this message.",
		guild.Name, reason,
	))
	if err != nil {
		return errors.Wrap(err, "sending message failed")
	}

	return nil
}
