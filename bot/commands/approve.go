package commands

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	heimdallr "gitlab.com/NorwegianLanguageLearning/heimdallr/bot"
)

var approveCommand = command{
	"approve",
	commandApprove,
	"Gives the user full access to the server.",
	[]string{
		"<user>",
	},
	[]string{
		"@username",
		"295207597929480192",
	},
}

func approveCommon(s *discordgo.Session, member *discordgo.Member, channelID string) (remPending bool, addUser bool, errPending error, errUser error) {
	if !isApproved(member) {
		errUser = s.GuildMemberRoleAdd(member.GuildID, member.User.ID, heimdallr.Config.UserRole)
		if errUser == nil {
			addUser = true
		} else {
			addUser = false
		}
	} else {
		addUser = false
		errUser = nil
	}

	if isPending(member) {
		errPending = s.GuildMemberRoleRemove(member.GuildID, member.User.ID, heimdallr.Config.PendingRole)
		if errPending == nil {
			remPending = true
		} else {
			remPending = false
		}
	} else {
		remPending = false
		errPending = nil
	}

	if !addUser {
		return
	}

	approvalMessage := heimdallr.Config.ApprovalMessage
	if len(approvalMessage) > 0 {
		if strings.Count(approvalMessage, "%s") > 0 {
			approvalMessage = fmt.Sprintf(approvalMessage, member.User.Mention())
		}
		_, err := s.ChannelMessageSend(channelID, approvalMessage)
		heimdallr.LogIfError(s, err)
	}

	return
}

// commandApprove gives a member the User role.
func commandApprove(s *discordgo.Session, m *discordgo.MessageCreate, args docopt.Opts) error {
	userID := getIDFromMaybeMention(args["<user>"].(string))

	guildID := m.GuildID
	member, err := heimdallr.GetMember(s, guildID, userID)
	if err != nil {
		_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("No member was found with ID %s.", userID))
		return errors.Wrap(err, "sending message failed")
	}

	_, _, errPending, errUser := approveCommon(s, member, m.ChannelID)
	if errPending != nil {
		heimdallr.LogIfError(s, errors.Wrap(errPending, "Failed to remove pending role."))
	}
	if errUser != nil {
		return errUser
	}

	return nil
}

// ReactionApprove approves a person if a mod reacts to their message with a green checkmark in the welcome channel
func ReactionApprove(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
	if m.ChannelID != heimdallr.Config.WelcomeChannel {
		return
	}

	if m.Emoji.Name != "✅" {
		return
	}

	reactingMember, err := heimdallr.GetMember(s, m.GuildID, m.UserID)
	if err != nil {
		heimdallr.LogIfError(s, err)
		return
	}
	guild, err := heimdallr.GetGuild(s, m.GuildID)
	if err != nil {
		heimdallr.LogIfError(s, err)
		return
	}
	if !heimdallr.IsModOrHigher(reactingMember, guild) {
		return
	}

	message, err := heimdallr.GetMessage(s, m.ChannelID, m.MessageID)
	if err != nil {
		heimdallr.LogIfError(s, err)
		return
	}
	member, err := heimdallr.GetMember(s, m.GuildID, message.Author.ID)
	if err != nil {
		heimdallr.LogMessage(s, fmt.Sprintf("Could not approve user *%s*. They have likely left the server.", message.Author.Username))
		return
	}

	_, _, errPending, errUser := approveCommon(s, member, m.ChannelID)
	if errPending != nil {
		heimdallr.LogIfError(s, errors.Wrap(errPending, "Failed to remove pending role."))
	}
	if errUser != nil {
		heimdallr.LogIfError(s, errors.Wrap(errUser, "Failed to add user role."))
	}
}

func isApproved(m *discordgo.Member) bool {
	for _, role := range m.Roles {
		if role == heimdallr.Config.UserRole {
			return true
		}
	}
	return false
}

func isPending(m *discordgo.Member) bool {
	if len(heimdallr.Config.PendingRole) == 0 {
		return false
	}

	for _, role := range m.Roles {
		if role == heimdallr.Config.PendingRole {
			return true
		}
	}
	return false
}
