package heimdallr

import (
	"database/sql"
	"github.com/pkg/errors"
	"time"

	"github.com/bwmarrin/discordgo"
	// Register SQL driver
	// _ "github.com/mattn/go-sqlite3"
	_ "modernc.org/sqlite"
)

// Infraction contains the reason and time for a user infraction
type Infraction struct {
	Reason string
	Time   time.Time
}

// Resource represents a learning resource
type Resource struct {
	ID      int
	Name    string
	Content string
	Tags    []string
}

var db *sql.DB

// OpenDb opens a connection to the database and creates the tables if they don't exist
func OpenDb(file string) error {
	var err error
	db, err = sql.Open("sqlite", file)
	if err != nil {
		return errors.Wrap(err, "opening database failed")
	}

	createTableStatement := `
CREATE TABLE IF NOT EXISTS users (
	id TEXT PRIMARY KEY,
	username TEXT
);

CREATE TABLE IF NOT EXISTS infractions (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	reason TEXT,
	time_ DATETIME,
	user_id TEXT,
	FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS invites (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	code TEXT,
	time_ DATETIME,
	user_id TEXT,
	FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS resources (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	content TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS resource_tags (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS resource_tags_resources (
	resource_id INTEGER,
	resource_tag_id INTEGER,
	PRIMARY KEY(resource_id, resource_tag_id),
	FOREIGN KEY(resource_id) REFERENCES resources(id),
	FOREIGN KEY(resource_tag_id) REFERENCES resource_tags(id)
);
`
	_, err = db.Exec(createTableStatement)
	return errors.Wrap(err, "creating database tables failed")
}

// CloseDb closes the database connection
func CloseDb() error {
	return errors.Wrap(db.Close(), "closing database failed")
}

// GetInfractions gets the list of infractions for a user
func GetInfractions(userID string) ([]Infraction, error) {
	var infractions []Infraction
	rows, err := db.Query(
		"SELECT reason, time_ FROM infractions WHERE user_id=$1 ORDER BY datetime(time_)",
		userID,
	)
	if err != nil {
		return infractions, errors.Wrap(err, "fetching infractions failed")
	}

	for rows.Next() {
		var infractionReason string
		var infractionTime time.Time
		err = rows.Scan(&infractionReason, &infractionTime)
		if err != nil {
			return nil, errors.Wrap(err, "parsing infraction row failed")
		}
		infractions = append(infractions, Infraction{infractionReason, infractionTime})
	}

	if err = rows.Err(); err != nil {
		return infractions, errors.WithStack(err)
	}

	return infractions, nil
}

// AddInfraction adds an infraction for a user
func AddInfraction(user discordgo.User, infraction Infraction) error {
	err := AddUser(user)
	if err != nil {
		return err
	}

	_, err = db.Exec("INSERT INTO infractions (reason, time_, user_id) VALUES ($1, $2, $3)",
		infraction.Reason, infraction.Time, user.ID)
	return errors.Wrap(err, "inserting infraction failed")
}

// AddInvite adds an invite for a user
func AddInvite(user discordgo.User, invite discordgo.Invite) error {
	err := AddUser(user)
	if err != nil {
		return err
	}

	_, err = db.Exec("INSERT INTO invites (code, time_, user_id) VALUES ($1, $2, $3)",
		invite.Code, invite.CreatedAt, user.ID)
	return errors.Wrap(err, "inserting user failed")
}

// AddUser adds a user or updates the username if it already exists
func AddUser(user discordgo.User) error {
	_, err := db.Exec("INSERT OR IGNORE INTO users (id) VALUES ($1)", user.ID)
	if err != nil {
		return errors.Wrap(err, "inserting user failed")
	}

	_, err = db.Exec("UPDATE users SET username=$1 WHERE id=$2", user.Username, user.ID)
	return errors.Wrap(err, "updating user failed")
}

// GetResourceByID gets a resource from the database by ID
func GetResourceByID(id int) (*Resource, error) {
	rows, err := db.Query(
		"SELECT resources.id, resources.name, content, resource_tags.name AS tag"+
			" FROM resources"+
			" 	LEFT JOIN resource_tags_resources ON resource_id = resources.id"+
			"	LEFT JOIN resource_tags ON resource_tag_id = resource_tags.id"+
			" WHERE resources.id = $1",
		id,
	)
	if err != nil {
		return nil, errors.Wrap(err, "getting resource by id failed")
	}
	resources, err := getResources(rows)
	if err != nil {
		return nil, err
	}
	return resources[0], nil
}

// GetResourceByName gets a resource frm the database by name
func GetResourceByName(name string) (*Resource, error) {
	rows, err := db.Query(
		"SELECT resources.id, resources.name, content, resource_tags.name AS tag"+
			" FROM resources"+
			" 	LEFT JOIN resource_tags_resources ON resource_id = resources.id"+
			"	LEFT JOIN resource_tags ON resource_tag_id = resource_tags.id"+
			" WHERE LOWER(resources.name) LIKE LOWER('%' || $1 || '%')",
		name,
	)
	if err != nil {
		return nil, errors.Wrap(err, "getting resource by name failed")
	}
	resources, err := getResources(rows)
	if err != nil {
		return nil, err
	}
	return resources[0], nil
}

// SearchResources searches the database for resources matching the search terms
func SearchResources(searchTerms []string) ([]*Resource, error) {
	query := "SELECT resources.id, resources.name, content, resource_tags.name AS tag" +
		" FROM resources" +
		" 	LEFT JOIN resource_tags_resources ON resource_id = resources.id" +
		"	LEFT JOIN resource_tags ON resource_tag_id = resource_tags.id" +
		" WHERE"
	var queryTerms []interface{}
	for i, searchTerm := range searchTerms {
		query += " LOWER(resources.name) LIKE LOWER('%' || ? || '%')" +
			"		OR LOWER(resources.content) LIKE LOWER('%' || ? || '%')" +
			"		OR LOWER(resource_tags.name) LIKE LOWER('%' || ? || '%')"
		if i < len(searchTerms)-1 {
			query += " OR"
		}
		for i := 0; i < 3; i++ {
			queryTerms = append(queryTerms, searchTerm)
		}
	}
	rows, err := db.Query(query, queryTerms...)
	if err != nil {
		return nil, errors.Wrap(err, "searching resources failed")
	}
	resources, err := getResources(rows)
	if err != nil {
		return nil, err
	}
	return resources, nil
}

func getResources(rows *sql.Rows) ([]*Resource, error) {
	var resources []*Resource
	resourcesByID := make(map[int]Resource)
	for rows.Next() {
		var id int
		var name, content string
		var tag sql.NullString
		err := rows.Scan(&id, &name, &content, &tag)
		if err != nil {
			return nil, errors.Wrap(err, "parsing resource row failed")
		}
		resource, ok := resourcesByID[id]
		if !ok {
			resource = Resource{
				ID:      id,
				Name:    name,
				Content: content,
			}
			resourcesByID[id] = resource
			resources = append(resources, &resource)
		}
		if tag.Valid {
			resource.Tags = append(resource.Tags, tag.String)
		}
	}

	if err := rows.Err(); err != nil {
		return nil, errors.WithStack(err)
	}

	return resources, nil
}

// AddResource adds a resource to the database
func AddResource(resource Resource) (int64, error) {
	tx, err := db.Begin()
	if err != nil {
		return -1, errors.Wrap(err, "starting transaction failed")
	}

	result, err := tx.Exec("INSERT INTO resources (name, content) VALUES ($1, $2)",
		resource.Name, resource.Content)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			err = errors.Wrap(rollbackErr, "rollback failed")
		} else {
			err = errors.Wrap(err, "inserting resource failed")
		}
		return -1, err
	}

	resourceID, err := result.LastInsertId()
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			err = errors.Wrap(rollbackErr, "rollback failed")
		} else {
			err = errors.Wrap(err, "getting id of inserted resource failed")
		}
		return -1, err
	}

	for _, tag := range resource.Tags {
		_, err = tx.Exec("INSERT OR IGNORE INTO resource_tags (name) VALUES ($1)", tag)
		if err != nil {
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				err = errors.Wrap(rollbackErr, "rollback failed")
			} else {
				err = errors.Wrap(err, "inserting resource tag failed")
			}
			return -1, err
		}

		result := tx.QueryRow("SELECT id FROM resource_tags WHERE name = $1", tag)
		var tagID int64
		if err = result.Scan(&tagID); err != nil {
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				err = errors.Wrap(rollbackErr, "rollback failed")
			} else {
				err = errors.Wrap(err, "getting id of resource tag failed")
			}
			return -1, err
		}

		_, err = tx.Exec("INSERT INTO resource_tags_resources (resource_id, resource_tag_id) VALUES ($1, $2)", resourceID, tagID)
		if err != nil {
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				err = errors.Wrap(rollbackErr, "rollback failed")
			} else {
				err = errors.Wrap(err, "inserting relationship between resource and resource tag failed")
			}
			return -1, err
		}
	}

	if err = tx.Commit(); err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			err = errors.Wrap(rollbackErr, "rollback failed")
		} else {
			err = errors.Wrap(err, "committing transaction failed")
		}
		return -1, err
	}

	return resourceID, nil
}
