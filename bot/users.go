package heimdallr

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"

	"regexp"

	"github.com/bwmarrin/discordgo"
)

// UserJoinHandler handles new users joining the server, and will welcome them.
func UserJoinHandler(s *discordgo.Session, g *discordgo.GuildMemberAdd) {
	if Config.BannedNames != nil {
		for _, name := range Config.BannedNames {
			matches, err := regexp.MatchString(name, g.User.Username)
			LogIfError(s, errors.Wrap(err, "Regex failed"))

			if matches {
				err = s.GuildBanCreateWithReason(g.GuildID, g.User.ID, fmt.Sprintf("Name was blacklisted. Regex: %s", name), 0)
				LogIfError(s, errors.Wrap(err, "Failed to ban user due to name blacklist"))
				_, err = s.ChannelMessageSend(Config.WelcomeChannel, fmt.Sprintf("User %s (`%s`) was banned due to matching the blacklist regex `%s`.", g.User.Mention(), g.User.Username, name))
				LogIfError(s, errors.Wrap(err, "sending message failed"))
			}

		}
	}
	welcomeMessage := Config.WelcomeMessage
	if strings.Count(welcomeMessage, "%s") > 0 {
		welcomeMessage = fmt.Sprintf(welcomeMessage, g.User.Mention())
	}
	_, err := s.ChannelMessageSend(Config.WelcomeChannel, welcomeMessage)
	LogIfError(s, errors.Wrap(err, "sending message failed"))
}

// UserLeaveHandler wishes ex members goodbye
func UserLeaveHandler(s *discordgo.Session, g *discordgo.GuildMemberRemove) {
	var name string
	if g.Nick != "" {
		name = g.Nick
	} else {
		name = g.User.Username
	}
	_, err := s.ChannelMessageSend(Config.WelcomeChannel, fmt.Sprintf("User `%s` (%s) has left the building.", name, g.User.Mention()))
	LogIfError(s, errors.Wrap(err, "sending message failed"))
}
